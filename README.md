# springboot-vue 前后端分离

#### 介绍
springboot-vue前后端分离，后台是mybatisplus+h2开发

#### 安装教程
后台启动

1.安装jdk8

2.打开cmd，输入 java -Dfile.encoding=utf-8 -jar D:\h2_mybatisplus_demo-2.3.5.RELEASE.jar

3.访问后台接口文档 http://localhost:9090/doc.html

连接h2数据库
http://localhost:9090/h2-console

账号：root  root

![输入图片说明](image.png)

