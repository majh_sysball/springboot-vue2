import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Person from '@/components/Person'
import UserList from '@/components/UserList'
import UserAdd from '@/components/UserAdd'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    }
    ,
    {
      path: '/Person',
      name: 'Person',
      component: Person
    }
    ,
    {
      path: '/UserList',
      name: 'UserList',
      component: UserList
    }
    ,
    {
      path: '/UserAdd',
      name: 'UserAdd',
      component: UserAdd
    }
  ]
})
