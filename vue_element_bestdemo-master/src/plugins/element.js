import Vue from 'vue'
import { 
    Container,
    Header,
    Aside,
    Main,
    Footer,

    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    
    Radio,
    RadioGroup,
    RadioButton,
    Checkbox,
    CheckboxGroup,
    CheckboxButton,
    Input,
    InputNumber,
    Select,
    Option,
    Switch,
    Button,

    Form,
    FormItem,

    Table,
    TableColumn,

    Pagination,

    Loading,

    Dialog,
    Tooltip,
    Popover,
    Popconfirm,
    Message,
    MessageBox,
 
    
} from 'element-ui'

//布局容器
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Footer)

//菜单
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)


//form 表单
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(CheckboxButton)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Select)
Vue.use(Option)
Vue.use(Switch)
Vue.use(Button)

Vue.use(Form)
Vue.use(FormItem)

//表格
Vue.use(Table)
Vue.use(TableColumn)

//分页
Vue.use(Pagination)

Vue.use(Loading)

Vue.use(Dialog)
Vue.use(Tooltip)
Vue.use(Popover)
Vue.use(Popconfirm)




Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$message = Message;


