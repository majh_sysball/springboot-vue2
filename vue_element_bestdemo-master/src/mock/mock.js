import Mock from 'mockjs'
Mock.mock('http://127.0.0.1/person', {
	'person|10': [{
		'name': '@cname',
		'gender|1': ['男', '女'],
		'phone|1': ['13531544954','13632250649','15820292420','15099905612'],
		'birthday': '@date'
	}]
})