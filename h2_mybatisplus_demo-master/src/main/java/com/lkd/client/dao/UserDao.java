package com.lkd.client.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lkd.client.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao extends BaseMapper<User>  {

}
