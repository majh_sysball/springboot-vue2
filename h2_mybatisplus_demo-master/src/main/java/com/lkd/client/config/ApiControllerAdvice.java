package com.lkd.client.config;

import com.lkd.client.pojo.Resp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ApiControllerAdvice {

    @ExceptionHandler(Exception.class)
    public Resp<Void> handleUnknownException1(Exception exception) {
        log.error("【系统错误】", exception);
        return new Resp("400", exception.getMessage(), null);
    }

    @ExceptionHandler(Throwable.class)
    public Resp<Void> handleUnknownException(Throwable exception) {
        log.error("【服务器错误】", exception);
        return new Resp("500", exception.getMessage(), null);
    }
}
