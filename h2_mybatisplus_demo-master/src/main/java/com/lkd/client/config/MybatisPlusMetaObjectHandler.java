package com.lkd.client.config;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

/**
 * MybatisPlus 自动填充配置
 */
@Component
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Date now = new Date();
        this.fillValIfNullByName("createAt", now, metaObject, false);
        this.fillValIfNullByName("updateAt", now, metaObject, false);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.fillValIfNullByName("updateAt", new Date(), metaObject, true);
    }

    /**
     * 填充值，先判断是否有手动设置，优先手动设置的值
     *
     * @param fieldName  属性名
     * @param fieldVal   属性值
     * @param metaObject MetaObject
     * @param isCover    是否覆盖原有值,避免更新操作手动入参
     */
    private void fillValIfNullByName(String fieldName, Object fieldVal, MetaObject metaObject, boolean isCover) {
        if (!metaObject.hasSetter(fieldName)) {
            return;
        }
        Object userSetValue = metaObject.getValue(fieldName);
        if (Objects.nonNull(userSetValue) && !isCover) {
            return;
        }
        Class<?> getterType = metaObject.getGetterType(fieldName);
        if (ClassUtils.isAssignableValue(getterType, fieldVal)) {
            metaObject.setValue(fieldName, fieldVal);
        }
    }
}
