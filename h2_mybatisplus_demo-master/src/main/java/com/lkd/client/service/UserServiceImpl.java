package com.lkd.client.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lkd.client.dao.UserDao;
import com.lkd.client.pojo.User;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> {

}
