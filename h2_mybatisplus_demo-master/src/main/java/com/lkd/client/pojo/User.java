package com.lkd.client.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Api(tags = "用户实体")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("student")
public class User {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String userName;
    private String sex;
    private Integer age;
    private String emails;
    private String qq;
    private String phone;
    private String address;
    private String school;
    private String major;
    private String title;
    private String type;
    private String dailyUrl;
    private String description;
    private String knowFrom;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createAt;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateAt;
}
