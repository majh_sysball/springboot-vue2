package com.lkd.client.pojo;

import lombok.Data;

@Data
public class Req<T> {
    private Integer pageSize = 10;
    private Integer curPage = 1;
    private T data;
}
