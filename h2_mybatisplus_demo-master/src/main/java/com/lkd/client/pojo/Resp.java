package com.lkd.client.pojo;

import lombok.Data;

@Data
public class Resp<T> {
    private String code;
    private String message;
    private T data;

    public Resp() {

    }

    public Resp(T data) {
        this.data = data;
    }

    public Resp(String code, T data) {
        this.code = code;
        this.data = data;
    }

    public Resp(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
