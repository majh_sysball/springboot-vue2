package com.lkd.client.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lkd.client.dao.UserDao;
import com.lkd.client.pojo.Req;
import com.lkd.client.pojo.Resp;
import com.lkd.client.pojo.User;
import com.lkd.client.service.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "用户模块")
@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserDao userDao;

    @ApiOperation("查询用户信息List")
    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    public Resp<List<User>> list(@RequestBody User user) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(user.getId()), User::getId, user.getId());
        queryWrapper.like(StringUtils.isNotBlank(user.getUserName()), User::getUserName, user.getUserName());
        queryWrapper.like(StringUtils.isNotBlank(user.getType()), User::getType, user.getType());
        queryWrapper.like(StringUtils.isNotBlank(user.getTitle()), User::getTitle, user.getTitle());
        queryWrapper.eq(StringUtils.isNotBlank(user.getSex()), User::getSex, user.getSex());
        queryWrapper.ge(user.getAge() != null, User::getAge, user.getAge());
        queryWrapper.eq(StringUtils.isNotBlank(user.getKnowFrom()), User::getKnowFrom, user.getKnowFrom());
        queryWrapper.orderByDesc(User::getCreateAt);

        return new Resp<>("200", userDao.selectList(queryWrapper));
    }

    @ApiOperation("分页查询用户信息")
    @RequestMapping(value = "/page", method = {RequestMethod.POST})
    public Resp<IPage<User>> page(@RequestBody Req<User> record) {
        User user = record.getData() != null ? record.getData() : new User();
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(user.getId()), User::getId, user.getId());
        queryWrapper.like(StringUtils.isNotBlank(user.getUserName()), User::getUserName, user.getUserName());
        queryWrapper.like(StringUtils.isNotBlank(user.getType()), User::getType, user.getType());
        queryWrapper.eq(StringUtils.isNotBlank(user.getKnowFrom()), User::getKnowFrom, user.getKnowFrom());
        queryWrapper.eq(StringUtils.isNotBlank(user.getSex()), User::getSex, user.getSex());
        queryWrapper.like(StringUtils.isNotBlank(user.getTitle()), User::getTitle, user.getTitle());
        queryWrapper.ge(user.getAge() != null, User::getAge, user.getAge());
        queryWrapper.orderByDesc(User::getCreateAt);

        IPage<User> list = userDao.selectPage(new Page(record.getCurPage(), record.getPageSize()), queryWrapper);
        return new Resp<>("200", list);
    }

    @ApiOperation("插入用户信息")
    @PostMapping("/insert")
    public Resp insert(@RequestBody User user) {
        return new Resp<>("200", userService.save(user));
    }

    @ApiOperation("修改用户信息")
    @PostMapping("/update")
    public Resp update(@RequestBody User user) {
        return new Resp<>("200", userService.updateById(user));
    }

    @ApiOperation("根据id删除用户信息")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST, RequestMethod.GET})
    public Resp delete(Long id) throws Exception {
        if (id == null) {
            throw new Exception("id不能为空");
        }
        return new Resp<>("200", userService.removeById(id));
    }

    @ApiOperation("根据id查询用户信息")
    @RequestMapping(value = "/selectById", method = {RequestMethod.POST, RequestMethod.GET})
    public Resp<User> selectById(Long id) throws Exception {
        if (id == null) {
            throw new Exception("id不能为空");
        }
        return new Resp<>("200", userDao.selectById(id));
    }
}
