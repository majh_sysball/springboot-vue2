package com.lkd.client.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lkd.client.dao.UserDao;
import com.lkd.client.pojo.Resp;
import com.lkd.client.pojo.User;
import com.lkd.client.service.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

@Api(tags = "登录模块")
@RestController
public class LoginController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserDao userDao;

    public static String token = getUUID();

    @ApiOperation("登录")
    @RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号", required = true),
            @ApiImplicitParam(name = "password", value = "密码", required = true)})
    public Resp login(String userName, String password, HttpServletResponse response) throws Exception {
        if (!"root".equals(userName) || !"root".equals(password)) {
            throw new Exception("账号或密码错误.root/root");
        }
        response.setHeader("token", token);
        return new Resp<>("200", "登录成功");
    }

    @ApiOperation("查询用户信息(header头要携带token)")
    @RequestMapping(value = "/getUserList", method = {RequestMethod.POST})
    public Resp<List<User>> getUserList(@RequestBody User user, HttpServletRequest request) throws Exception {
        String token1 = request.getHeader("token");
        if (StringUtils.isBlank(token1)) {
            throw new Exception("header没有token,没有权限访问");
        }
        if (!token.equals(token1)) {
            throw new Exception("header的token不正确,没有权限访问");
        }
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(user.getId()), User::getId, user.getId());
        queryWrapper.like(StringUtils.isNotBlank(user.getUserName()), User::getUserName, user.getUserName());
        queryWrapper.like(StringUtils.isNotBlank(user.getType()), User::getType, user.getType());
        queryWrapper.like(StringUtils.isNotBlank(user.getTitle()), User::getTitle, user.getTitle());
        queryWrapper.eq(StringUtils.isNotBlank(user.getSex()), User::getSex, user.getSex());
        queryWrapper.ge(user.getAge() != null, User::getAge, user.getAge());
        queryWrapper.eq(StringUtils.isNotBlank(user.getKnowFrom()), User::getKnowFrom, user.getKnowFrom());
        queryWrapper.orderByDesc(User::getCreateAt);
        return new Resp<>("200", userDao.selectList(queryWrapper));
    }

    public static String getUUID() {
        String id = UUID.randomUUID().toString();
        String uid = id.replaceAll("-", "");
        return uid;
    }
}
